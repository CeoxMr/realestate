<?php
require_once 'core/models.class.php';

class main_model extends models {

    private $db;

    public function __construct() {
        $this->db = $this->connect();
    }

    /**
     * @return array With the sidebar settings as a json object.
     */
    public function read_sidebar(): array {
        try {

            $sections = $this->db->query('SELECT * FROM menu_sections order by `order`');
            $sections->execute();
            $sections =$sections->fetchAll(PDO::FETCH_ASSOC);

            $options = $this->db->query('SELECT * FROM menu_options order by `order`');
            $options->execute();
            $options = $options->fetchAll(PDO::FETCH_ASSOC);

            foreach ($options as $option){
                foreach ($sections as $k => $v){
                    if($option['parent'] === $sections[$k]['id']){
                        $sections[$k]['child'][] = $option;

                    }
                }
            }

            return $sections;
        }catch(PDOException $e){
            throw new \RuntimeException($e->getMessage());
        }
    }

    /**
     * @return array with pages href and access requirement.
     */
    public function read_site_pages(): array {

        try {
            $pages = $this->db->query('SELECT href, access FROM menu_sections UNION SELECT href, access FROM menu_options');
            $pages->execute();

            return $pages->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new \RuntimeException($e->getMessage());
        }


    }
}
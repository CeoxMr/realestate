<?php
require_once 'core/models.class.php';

class users_model extends models
{
    public  $user;
    public  $errMessage;
    private $db;

    public function __construct() {
        $this->db = $this->connect();
    }

    /**
     * This method retrieves the users's and users's profile values from the database.
     * @param string $user to find the users in the database.
     * @param bool $password TRUE for the password to be included into the array | FALSE [DEFAULT] to remove the password from the array.
     * @return bool|mixed Associative array with all the information pertaining to the users and his profile.
     * @throws Exception
     */
    public function fetch_user($user=null, $password=False){
        try {
            $statement = 'SELECT * FROM users INNER JOIN user_profile profile ON users.ID=profile.User_id AND users.Username=:username OR users.ID=:id';

            $filter = is_numeric($user)? PDO::PARAM_INT : PDO::PARAM_STR;

            $stmt = $this->db->prepare($statement);
            $stmt->bindValue(':username', $user, $filter);
            $stmt->bindValue(':id', $user, $filter);
            $stmt->execute();

            if ($stmt->rowCount() > 0) {

                $user = $stmt->fetch(PDO::FETCH_ASSOC);
                $user['FullName'] = $user['Name'] . ' ' . $user['Surname'];
                $user['Role'] = $this->fetch_user_role($user['Role']);
                $user['Picture'] = (file_exists(ltrim($user['Picture'], '/'))) ? $user['Picture'] : '/assets/img/image-not-available.jpg';
                $user['Social'] = json_decode($user['Social'], TRUE);
                if (!$password) {
                    unset($user['Password']);
                }
                return $user;
            }
            throw new RuntimeException('Unable to find users.');
        }catch (PDOException $e){
            throw new RuntimeException($e->getCode() . ': Unable to retrieve user\'s information. Please try again.');
        }
    }

    /**
     * @param array $filters of values
     * @return array
     */
    public function dt_read_users($filters=[]): array {
        try {
            // We make sure that both the direction and the column name are correct values. direction has to be either asc or desc and the column has to match with a table column.
            if(in_array($filters['order'][0]['dir'],['asc', 'desc'], true) && in_array($filters['order'][0]['column'], $this->get_table_columns('users'), true)){
                // If they match then we replace the array in order for a string to be used directly into the query.
                $filters['order'] = "{$filters['order'][0]['column']} {$filters['order'][0]['dir']} ";
            }else{
                // if they don't match we send a default value.
                $filters['order'] = 'ID ASC ';
            }
            // We set the query, prepare it and execute it. NOTE: Had to hard code the order because there was no way to pass it with a placeholder.
            $stmt['query'] = "
                            SELECT u.ID, Username, u.Name, Surname, Email, r.Name as Role
                            FROM users u LEFT JOIN user_roles r on u.Role = r.ID
                            WHERE u.Username LIKE :search or u.Email LIKE :search or u.Name LIKE :search or u.Surname LIKE :search or r.Name LIKE :search
                            ORDER BY {$filters['order']} 
                            LIMIT :start,:length";
            $users['data'] = $this->db->prepare($stmt['query']);

            $users['data']->bindValue(':search', "%{$filters['search']}%", PDO::PARAM_STR);
            $users['data']->bindValue(':start', (int)$filters['start'], PDO::PARAM_INT);
            $users['data']->bindValue(':length', (int)$filters['length'], PDO::PARAM_INT);
            $users['data']->execute();
            // We then get the total of the records in the users table.
            $users['recordsTotal'] = $this->db->query('SELECT count(*) FROM users');
            $users['recordsTotal']->execute();
            // We assign all the values retrieved from the database.
            $users['recordsTotal'] = $users['recordsTotal']->fetch(PDO::FETCH_NUM)[0];
            $users['recordsFiltered'] = $users['data']->rowCount();
            $users['data'] = $users['data']->fetchAll(PDO::FETCH_ASSOC);

            return $users;

        }catch(PDOException $e){
            throw new \RuntimeException($e->getCode() . ': No users found.');
        }
    }

    public function get_username($user_id=FALSE) {
        try {
            $user_id = $user_id ? filter_var($user_id, FILTER_SANITIZE_NUMBER_INT) : $_SESSION['users']['ID'];

            $statement = 'SELECT Username FROM users WHERE ID=:id LIMIT 1';
            $stmt = $this->db->prepare($statement);
            $stmt->bindValue(':id', $user_id, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->rowCount() > 0){
                $user = $stmt->fetch(PDO::FETCH_ASSOC);
                return $user['Username'];
            }
        }catch (PDOException $e){
            throw new RuntimeException($e->getCode() . ': Unable to retrieve username from the database.');
        }
        return false;
    }

    /**
     * This method retrieves users's messages from the database.
     * @param int $user_id  to find his messages.
     * @param int $limit    Amount of notifications to be retrieved.
     * @param string $order [DESC / ASC] Order in which the messages will be retrieved.
     * @return array        as associative with all the messages retrieved.
     * @throws Exception    if no messages were found.
     */
    public function fetch_messages($user_id=0, $limit=0, $order='DESC'): array {
        $id = ($user_id > 0) ? $user_id : $_SESSION['users']['ID'];

        $statement = 'SELECT * FROM user_messages WHERE User_id=:user_id';

        if ($order === 'DESC') {
            $statement .= ' ORDER BY Sent_on ' . $order;
        }

        if ($order === 'ASC') {
            $statement .= ' ORDER BY Sent_on ' . $order;
        }

        if ($limit > 0) {
            $statement .= ' LIMIT ' . $limit;
        }

        $messages = $this->db->prepare($statement);
        $messages->bindParam(':user_id', $id, PDO::PARAM_INT);
        $messages->execute();

        if ($messages->rowCount() > 0){
            return $messages->fetchAll(PDO::FETCH_ASSOC);
        }

        throw new RuntimeException('No messages were found');
    }

    /**
     * This method retrieves the users's notifications from the database
     * @param int $user_id  to find his notifications.
     * @param int $limit    Amount of notifications to be retrieved.
     * @return array        Associative array with all the notifications retrieved | False
     * @throws Exception    if no notifications were found.
     */
    public function fetch_notifications($user_id=0, $limit=0): array {
        $user_id = ($user_id > 0) ? $user_id : $_SESSION['users']['ID'];

        $statement = 'SELECT * FROM user_notifications WHERE User_id=:user_id ORDER BY Date DESC';

        if($limit > 0) {
            $statement .= ' LIMIT ' . $limit;
        }

        $notifications = $this->db->prepare($statement);
        $notifications->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $notifications->execute();

        if ($notifications->rowCount() > 0){
            return $notifications->fetchAll(PDO::FETCH_ASSOC);
        }

        throw new RuntimeException('No notifications where found');
    }

    /**
     * This method retrieves a role by it's ID from the database.
     * @param int $role_id      User's role id to search within the database.
     * @return array            With the users's role.
     * @throws RuntimeException If unable to find role.
     */
    public function fetch_user_role($role_id=0): array
    {
        $statement = 'SELECT * FROM  user_roles WHERE ID=:role_id LIMIT 1';
        $roles = $this->db->prepare($statement);
        $roles->bindValue(':role_id', $role_id, PDO::PARAM_INT);
        $roles->execute();
        $role = $roles->fetch(PDO::FETCH_ASSOC);
        if($roles->rowCount() === 1){
            return $role;
        }
        throw new RuntimeException('Unable to find role.');
    }

    /**
     * This method returns all the roles stored in the database.
     * @return array            with all the roles.
     * @throws RuntimeException if no roles were found.
     */
    public function fetch_roles (): array
    {
        //
        $statement = 'SELECT * FROM  user_roles';
        $roles = $this->db->query($statement);
        //
        if($roles->rowCount() > 0){
            return $roles->fetchAll(PDO::FETCH_ASSOC);
        }
        //
        throw new RuntimeException('No roles where found.');
    }

    /**
    * This method is to update User's information... This doesn't update User's profile information.
     * @param int $user_id      so that the right users is updated.
     * @param array $fields     ['Column' => 'Value'] This is an associative array with all the values to be updated.
     * @return bool             False if the execution fails.
     * @throws RuntimeException If unable te update the users.
     */
    public function update_user($user_id=0, $fields=array()): bool {
        try {
            $fields = $this->is_a_table_column(['users', 'user_profile'], $fields);
            $data = '';

            if (count($fields) > 1){
                foreach ($fields as $k => $v){
                    $data .= "{$k}=:{$k}, ";
                }
                $data = rtrim($data, ', ');
            }else{
                foreach ($fields as $k => $v){
                    $data .= "{$k}=:{$k}";
                }
            }
            $statement = "UPDATE users INNER JOIN user_profile up ON users.ID = up.User_id SET {$data} WHERE users.ID=:user_id";

            $stmt = $this->db->prepare($statement);

            foreach ($fields as $k => $v){
                $placeholder = ':' . $k;
                $stmt->bindValue($placeholder, $v);
            }
            $stmt->bindValue(':user_id', $user_id);

            if($stmt->execute()){
                return true;
            }
        } catch (PDOException $e){

            throw new RuntimeException($e->getCode().': Unable to update users, please contact your technical support.');
        } catch (RuntimeException $e){

            throw new RuntimeException($e->getMessage());
        }
        return false;
    }

}
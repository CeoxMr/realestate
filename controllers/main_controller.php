<?php /** @noinspection ForgottenDebugOutputInspection */
require_once 'core/controllers.class.php';

class main extends controllers
{
    /**
     * This is the method for the index page.
     */
    public function home(){
        $this->view->set_view_params([
            'title' => 'Dashboard',
            'js_files' => ['assets/vendor/chart.js/Chart.min.js', 'assets/vendor/chart.js/Chart.bundle.min.js', 'assets/js/demo/chart-area-demo.js', 'assets/js/demo/chart-bar-demo.js', 'assets/js/demo/chart-pie-demo.js']
        ]);
        //
        $this->view->render_view('index');
    }

    /**
     * This is just the method for the login page.
     */
    public function login(){
        //
        try {
            if (isset($_SESSION['users']) || $this->update_session()) {
                header('Location: /');
            }
        }catch (Exception $e){
            $this->errMessage = $e->getMessage();
        }
        //
        $this->view->set_view_params([
            'title' => 'Login',
            'errMessage' => $this->errMessage ?? NULL,
            'show_header' => false,
            'show_footer' => false
        ]);
        $this->view->render_view('users/login');
    }

    /**
     * This is the method for the logout page.
     */
    public function logout(){
        session_unset();
        header('Location: /');
    }

}
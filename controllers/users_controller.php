<?php

require_once 'core/controllers.class.php';

class users extends controllers
{

    private $model;

    public function home(): void {
        $this->view->set_view_params([
            'title'     =>'Manejo de usuarios',
            'css_files' =>['assets/vendor/datatables/dataTables.bootstrap4.css'],
            'js_files'  =>['assets/vendor/datatables/jquery.dataTables.js', 'assets/vendor/datatables/dataTables.bootstrap4.js','assets/js/users_datatable.js']
        ]);
        $this->view->render_view('users/main');
    }

    /**
     * @param array params
     * @throws Exception
     */
    public function profile($params=array()): void {

        // We check if users id has been received as a parameter
        $user_id = (!empty($params) && $_SESSION['users']['Role']['Access'] > 3 && is_numeric($params[0])) ? (int)$params[0] : $_SESSION['users']['ID'];

        try {
            $this->model = $this->load_model('users');

            if (!empty($_POST)) {
                $this->user = $this->filter_user_inputs($_POST);
                $this->model->update_user($user_id, $this->user);
            }elseif (!empty($_FILES)) {
                $this->user['Picture'] = $this->upload_picture($_FILES,$this->model->get_username($user_id),'profile');
                $this->model->update_user($user_id, $this->user);
            }

        }catch (Exception $e){
            $this->errMessage = $e->getMessage();
        }

        try {
            $this->user = $this->model->fetch_user($user_id);
        }catch (Exception $e){
            $this->errMessage = $e->getMessage();
        }

        if(!empty($this->user)){
            $_SESSION['users'] = $this->user;
            $this->view->set_view_params([
                'title' => $this->user['FullName'].' - Profile',
                'user'  => $this->user,
                'roles' => $this->model->fetch_roles(),
                'errMessage' => $this->errMessage ?? NULL
            ]);
            $this->view->render_view('users/profile');
        }else{
            $this->view->render_view('404');
        }
    }

    public function api(): void {
        // Load model we'll be working with
        $model = $this->load_model('users');

        $accepted_values = ['draw', 'start', 'length','search','order','columns'];
        $filtered_values = [];

        foreach ($_POST as $k => $v){
            if(in_array($k, $accepted_values, true)){
                switch ($k){
                    case 'draw':
                    case 'start':
                    case 'length':
                    case 'limit':
                        $v = (int)filter_var($v, FILTER_SANITIZE_NUMBER_INT);
                        break;
                    case 'search':
                        $v = filter_var($v['value'], FILTER_SANITIZE_STRING);
                        break;
                    case 'order':
                        foreach($v as $k2 => $a){
                            $v[$k2]['column'] = filter_var($_POST['columns'][$a['column']]['data'], FILTER_SANITIZE_STRING);
                        }
                        break;
                }
                $filtered_values[$k] = $v;
            }
        }

        if(empty($filtered_values)){
            $this->view->render_view('404');
            return;
        }

        $users = $model->dt_read_users($filtered_values);
        echo json_encode($users, true);
    }

    private function filter_user_inputs($post){
        // Patterns to compare inputs with.
        $date_pattern = '/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/';
        $phone_pattern = '/^\d{3}-\s?\d{3}-\d{4}$/';

        // User table info
        $filtered['Username'] = (!empty($post['Username'])) ? filter_var($post['Username'], FILTER_SANITIZE_STRING) : NULL;
        $filtered['Email'] = (!empty($post['Email'])) ? filter_var($post['Email'], FILTER_SANITIZE_EMAIL) : NULL;
        $filtered['Password'] = (!empty($post['Password'])) ? password_hash($post['Password'], PASSWORD_BCRYPT) : NULL;
        $filtered['Name'] = (!empty($post['Name'])) ? filter_var($post['Name'], FILTER_SANITIZE_STRING) : NULL;
        $filtered['Surname'] = (!empty($post['Surname'])) ? filter_var($post['Surname'], FILTER_SANITIZE_STRING) : NULL;
        $filtered['Role'] = (!empty($post['Role'])) ? filter_var($post['Role'], FILTER_SANITIZE_NUMBER_INT) : NULL;
        $filtered['Date'] = (!empty($post['Date']) && preg_match($date_pattern, $post['Date']) ) ? $post['Date'] : NULL;

        // User_profile info
        $filtered['Picture'] = (!empty($post['Picture'])) ? filter_var($post['Picture'], FILTER_SANITIZE_STRING) : NULL;
        $filtered['Biography'] = (!empty($post['Biography'])) ? filter_var($post['Biography'], FILTER_SANITIZE_STRING) : NULL;
        $filtered['Phone'] = (!empty($post['Phone'])  && preg_match($phone_pattern, $post['Phone'])) ? $post['Phone'] : NULL;
        $filtered['Mobile'] = (!empty($post['Mobile'])  && preg_match($phone_pattern, $post['Mobile'])) ? $post['Mobile'] : NULL;

        // User_profile social info
        $filtered['Social'] = [];
        $filtered['Facebook'] = (!empty($post['Facebook'])) ? array_push($filtered['Social'], filter_var($post['Facebook'], FILTER_SANITIZE_URL)) : NULL;
        $filtered['Instagram'] = (!empty($post['Instagram'])) ? array_push($filtered['Social'], filter_var($post['Instagram'], FILTER_SANITIZE_URL)) : NULL;
        $filtered['Twitter'] = (!empty($post['Twitter'])) ? array_push($filtered['Social'], filter_var($post['Twitter'], FILTER_SANITIZE_URL)) : NULL;
        $filtered['Pinterest'] = (!empty($post['Pinterest'])) ? array_push($filtered['Social'], filter_var($post['Pinterest'], FILTER_SANITIZE_URL)) : NULL;

        foreach ($filtered as $k => $v) {
            if ($v === NULL) {
                unset($filtered[$k]);
            }
        }

        if(isset($filtered['Role']) && $_SESSION['user']['Role']['Access'] < 4) {
            unset($filtered['Role']);
        }

        if (isset($filtered['Password']) && !password_verify($post['PasswordVerify'], $filtered['Password'])) {
            unset($filtered['Password']);
        }

        if(!empty($filtered['Social'])){
            $filtered['Social'] = json_encode($filtered['Social']);
            unset($filtered['Facebook'], $filtered['Instagram'], $filtered['Twitter'], $filtered['Pinterest']);
        }else{
            unset($filtered['Social']);
        }

        return $filtered;
    }
}
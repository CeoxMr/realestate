<?php
require_once 'core/controllers.class.php';

class settings extends controllers {

    public function sidebar(){

        $this->view->render_view('settings/sidebar', [
            'title'=>'Edit sidebar',
            'description'=>'Here you can setup your sidebar settings.'
        ]);
    }
}
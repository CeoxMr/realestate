<?php
require_once 'core/controllers.class.php';

/**
 * Class properties
 */
class properties extends controllers
{
    /**
     * @var string $default_title
     */
    public $default_title = 'My Properties';

    /**
     *  This function renders the main properties view with it custom scripts and css.
     */
    public function home(){

        $this->view->set_view_params([
            "title"         =>$this->default_title,
            "description"   =>'List of properties'
        ]);
        $this->view->set_custom_js(['assets/vendor/datatables/jquery.dataTables.min.js', 'assets/vendor/datatables/dataTables.bootstrap4.min.js', 'assets/js/demo/datatables-demo.js']);
        $this->view->set_custom_css(['assets/vendor/datatables/dataTables.bootstrap4.min.css']);

        $this->view->render_view('properties/main');
    }

    /**
     *
     */
    public function add(){

        $this->view->set_view_params([
            'title' => 'Add a property',
            'description' => 'In this page you\'ll find all you need to get your property online'
        ]);

        $this->view->render_view('properties/add');
    }

    /**
     * @param $property_id int The identifier to the property wished to edit.
     */
    public function edit($property_id){

        $this->view->set_view_params([
            'title' => 'Edit property',
            'description' => 'In this page you\'ll find all you need to get your property online'
        ]);

        $this->view->render_view('properties/edit');
    }
}
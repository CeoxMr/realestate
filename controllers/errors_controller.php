<?php
require_once 'core/controllers.class.php';

class errors extends controllers
{
    public function forbidden(){
        $this->view->render_view('403');
    }

    public function notfound(){
        $this->view->render_view('404');
    }
}
$(document).ready(function() {

    $('#users_datatable').DataTable( {
        processing: true,
        serverSide: true,
        ajax: {
            url: '/users/api/',
            type: 'POST'
        },
        columns: [
            { "data": "ID" },
            { "data": "Username" },
            { "data": "Name" },
            { "data": "Surname" },
            { "data": "Email" },
            { "data": "Role" }
        ]
    } );
} );
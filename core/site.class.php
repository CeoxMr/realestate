<?php session_start();
require_once 'controllers/errors_controller.php';

class site {

    private $default_controller = 'main';

    public function __construct() {
        $url = $this->getUrl();
        $this->load_controller($url);
    }

    private function getUrl(): array {
        // First we get the URL that has been sent using $_GET and Sanitize it.
        $url = (isset($_GET['url'])) ? filter_var($_GET['url'], FILTER_SANITIZE_URL) : null;

        // Check the whole array to remove any undesired characters allowing just letters a-z A-z and numbers 0-9.
        $url = preg_replace('/[^a-zA-Z0-9\/]+/', '', $url);

        // We remove any extra / that could be mistyped.
        $url = rtrim($url, '/');

        // We split the url string using the / into an array.
        $url = explode('/', $url);

        return $url;
    }

    /**
     * @param $url array contains the controller name on the first position, a method on the second, and any desired params from the third onwards.
     * @return bool
     */
    private function load_controller($url): bool {
        // We confirm if session has been started
        if(!isset($_SESSION['users']) && $url[0] !== 'login') {
            $url[0] = 'login';
        }

        // We check if a controller hasn't been received or if there is no session started.
        if(empty($url[0]) || $url[0] === 'login' || $url[0] === 'logout'){
            if($url[0] === 'login' || $url[0] === 'logout') {
                $url[1] = $url[0];
            }
            // If there is no controller received then we load the default one.
            $url[0] = $this->default_controller;
            $controller = 'controllers/' . $this->default_controller . '_controller.php';
        }else{
            // If the controller has been defined and an users is logged in then we load that controller.
            $controller = 'controllers/' . $url[0] . '_controller.php';
        }

        // Check if any method and/or parameters have been set and declares the variable that will contain the array with the parameters for it.
        $method = $url[1] ?? null;
        $parameters = array_slice($url, 2);

        // We confirm that the controller file does exist. We include the file and create the new controller.
        if (file_exists($controller)) {
            if((new controllers)->has_access()){
                /** @noinspection PhpIncludeInspection */
                require_once $controller;
                $controller = new $url[0];
                $this->load_method($controller, $method, $parameters);
            }
        }else {
            $controller = new errors();
            $controller->notfound();
            return false;
        }

        return true;
    }

    /**
     * @param $class_object object The current class object. [$this]
     * @param $method string The method to load.
     * @param $parameters array All the parameters to be used by the method loaded.
     */
    private function load_method($class_object, $method, $parameters=[]): void {
        if(method_exists($class_object, $method)){
            if(!empty($parameters)) {
                $class_object->{$method}($parameters);
            }else{
                $class_object->{$method}();
            }
        }
        elseif(isset($method)){
            $controller = new errors();
            $controller->notfound();
        }
        else {
            $class_object->home();
        }
    }

}
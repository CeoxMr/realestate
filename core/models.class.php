<?php


/**
 * @property PDO|string db
 */
class models
{
    private $host = 'localhost';
    private $username = 'root';
    private $password = '';
    private $dbname = 'realestate_project';

    /**
     * @return PDO|string
     */
    public function connect(){
        try{
            $conn = 'mysql:host='.$this->host.';dbname='.$this->dbname.';';
            $options = [
                PDO::ATTR_ERRMODE           => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES  => true,
            ];
            return new PDO($conn, $this->username, $this->password, $options);
        }catch (PDOException $e){
            return $e->getMessage();
        }
    }

    /**
     * @param string $table
     * @return array|null
     */
    public function get_table_columns($table=NULL): ?array
    {
        $table = filter_var($table, FILTER_SANITIZE_STRING);

        if ($table) {
            $db = $this->connect();
            $columns = $db->query("DESCRIBE {$table}");
            return $columns->fetchAll(PDO::FETCH_COLUMN);
        }

        return NULL;
    }

/**
 * Compares an array with given parameters with the column names of a table to remove any unmatched values.
 * @param string|array $table   Name of the table to be consulted.
 * @param array $parameters     To be filtered by verifying they
 * @return array                An associative array with the parameters, if parameters match with the table columns.
 * @throws RuntimeException     If no table is define or parameters are given.
 */
    protected function is_a_table_column($table,$parameters): array
    {
        $table_columns = null;
        if (!empty($parameters) && !empty($table)){
            if(!is_array($table)){
                $table_columns = $this->get_table_columns($table);
            }else{
                $table_columns = array();
                foreach ($table as $v){
                    /** @noinspection SlowArrayOperationsInLoopInspection */
                    $table_columns = array_merge($table_columns, $this->get_table_columns($v));
                }
            }

            $filtered_array = array();

            foreach ($parameters as $k => $v) {
                if(in_array($k, $table_columns, true)){
                    $filtered_array[$k] = $v;
                }
            }

            return $filtered_array;
        }

        throw new RuntimeException('No tables found or parameters given');
    }
}
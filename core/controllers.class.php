<?php


class controllers
{
    protected $user;
    protected $view;
    protected $errMessage;

    public function __construct()
    {
        $this->check_session();
        $this->view = new views();
        $this->load_sidebar();
    }

    /**
     * This function loads a module from a list of modules by the given parameters.
     * @param $model
     * @return object
     * @throws RuntimeException
     */
    protected function load_model($model) {
        if(file_exists('models/'.$model.'_model.php')){
            // Switching between models depending on which model has been requested.
            switch ($model){
                case 'main':
                    require_once 'models/main_model.php';
                    return new main_model();
                    break;
                case 'users':
                    require_once 'models/users_model.php';
                    return new users_model();
                    break;
                case 'properties':
                    require_once 'models/properties_model.php';
                    return new properties_model();
                    break;
                default:
                    $model .= '_model';
                    require_once "models/{$model}.php";
                    return new $model();
                    break;
            }
        }
        throw new RuntimeException('Unable to retrieve information from the database.');
    }

    /**
     * This function receives the JSON setting for the sidebar and converts it into an associative array to send it into the view.
     */
    protected function load_sidebar(): void {

        $model = $this->load_model('main');
        $settings = $model->read_sidebar();

        $this->view->set_sidebar_settings($settings);
    }

    /**
     * This function sets the user session if username and password receive from the login form are correct and match with the ones stored in the database.
     * @return bool         True if the login was successful and the $_SESSION['users'] has been set.
     * @throws Exception    Message if username or password were wrong or an error with the db connection happened.
     */
    protected function update_session(): bool {
        try {
            if (isset($_POST['username'])) {
                // Sanitizing the form input
                $username = filter_var($_POST['username'],FILTER_SANITIZE_STRING);
                // Loading the users model.
                $model = $this->load_model('users');
                // Retrieving the users's information.
                $user = $model->fetch_user($username, TRUE);
                // Verifying that the users was found.
                if (isset($user['Username'])) {
                    // Verifying password is correct.
                    if (password_verify($_POST['password'], $user['Password'])) {
                        // We remove the password string from the users object before assigning it to the current session.
                        unset($user['Password']);
                        // Assigning the current time to the users object
                        $_SESSION['last_activity'] = $_SERVER['REQUEST_TIME'];
                        // Updating users Last_session on the database.
                        $model->update_user($user['ID'], ['Last_session' => date('Y-m-d H:i:s', $_SESSION['last_activity'])]);
                        // Assigning all the users info to the session variables.
                        $_SESSION['users'] = $user;
                        return true;
                    }
                    throw new RuntimeException('<strong>Wrong Password</strong>, please try again...');
                }
                // Getting the users information
                throw new RuntimeException('<strong>Username not found</strong>, please try again...');
            }
            return false;
        }catch(Exception $e){
            throw new RuntimeException($e->getMessage());
        }
    }

    public function check_session(): void
    {

        $current_time   = $_SERVER['REQUEST_TIME'];
        $last_activity  = $_SESSION['last_activity']??NULL;
        $time_limit     = 1800;

        if($_SERVER['REQUEST_URI'] !== '/login/'){
            if(($current_time - $last_activity) >= $time_limit){
                session_unset();
                session_destroy();
                header('Location:/login/');
            }else{
                $_SESSION['last_activity'] = $current_time;
            }
            try {
                $model  = $this->load_model('users');
                $user   = $model->fetch_user($_SESSION['users']['Username']);

                $_SESSION['users'] = $user;
                $_SESSION['messages'] = $model->fetch_messages($user['ID'], 5);
                $_SESSION['notifications'] = $model->fetch_notifications($user['ID'], 5);
            } catch (Exception $e) {
                throw new RuntimeException($e->getMessage());
            }
        }
    }

    /**
     * This function returns true if current user access meets the access requirement provided.
     * @param int $access_requirement Access level required to compare with current user's role access.
     * @return bool
     */
    public function has_access(int $access_requirement=0): bool {

        $model      = $this->load_model('main');
        $pages      = $model->read_site_pages();
        $uri        = rtrim($_SERVER['REQUEST_URI'], '/');
        $access     = $_SESSION['users']['Role']['Access'] ?? 0;

        foreach ($pages as $page){
            $page['href'] = rtrim($page['href'], '/');
            if ($page['href'] === $uri && $page['access'] <= $access) {
                return true;
            }
        }

        $this->view->render_view('403');
        die();
    }

    /**
     * This function receives picture file being uploaded, makes sure is an image, change it's name and uploads it to a users's folder.
     * @param array $files      Just pass the picture array as received.
     * @param string $username  The username of the users who owns this file.
     * @param string $dir_name  The name of the target directory where file will be stored if directory doesn't exist it will be created under a directory with the users's username.
     * @return string           File new path ready to be stored in the database.
     * @throws Exception        If file is not an image or couldn't be uploaded.
     */
    protected function upload_picture($files, $username, $dir_name): string {
        $tmp_file       = $files['Picture']['tmp_name'];
        $file_type      = $files['Picture']['type'];
        $target_dir     = strtolower('uploads/' . $username . '/' . $dir_name);

        if(!$img_size = @getimagesize($tmp_file)){
            throw new RuntimeException('The file being uploaded is not a valid image file. Unable to upload');
        }

        if(!file_exists($target_dir)){
            $create = mkdir($target_dir, 0755, true);
        }

        $counter = 1;
        $file_num = array();
        foreach(glob($target_dir.'/{*.jpg,*.jpeg,*.png}', GLOB_BRACE) as $c){
            $filename   = pathinfo($c)['filename'];
            $file_num[] = explode('_',$filename)[1];
        }
        $counter += $file_num ? max($file_num) : 0;

        if ($file_type === 'image/jpg' || $file_type === 'image/jpeg' || $file_type === 'image/png') {

            $target_file = $target_dir. '/' . "img_{$counter}.jpg";

            if(move_uploaded_file($tmp_file, $target_file)){
                return '/' . $target_file;
            }

            throw new RuntimeException('An error occurred, unable to upload picture.');
        }
        throw new RuntimeException('This file is not recognized as a picture, unable to upload file.');
    }
}
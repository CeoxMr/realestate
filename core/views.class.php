<?php


class views
{
    private $host = 'http://realestate.test/';

    private $show_header;
    private $show_footer;
    private $show_sidebar = TRUE;
    private $js_files = [];
    private $css_files = [];

    /**
     * You can set the parameters to be used within your view as an associative array. Converts each key=>value to a normal variable that can be called from any _view using $this->key.
     * @param $parameters array "title"=>String, "description"=>String, "header"=>boolean, "footer"=>boolean, "js_files"=>[Paths to custom js files], "css"=>[Paths to custom css files]
     */
    public function set_view_params($parameters): void {
        foreach ($parameters as $k => $val) {
            $this->{$k} = $val;
        }
    }

    public function set_sidebar_settings($settings = []): void {
        if(!empty($settings)){
            $this->sidebar = $settings;
        }else{
            throw new RuntimeException('No sidebar settings given.');
        }
    }

    /**
     * You can set your javascript files from an array containing the paths to the scripts.
     * @param array $js_files with all the paths to the different javascript files wished to include.
     */
    public function set_custom_js($js_files): void {
        $this->js_files = $js_files;
    }

    /**
     * You can set your css files from an array containing the paths to the stylesheets.
     * @param array $css_files with all the paths to the different css files with to include.
     */
    public function set_custom_css($css_files = array()): void {
        $this->css_files = $css_files;
    }

    /**
     * @return string an html script tag for each custom javascript files defined using view::set_view_params().
     */
    public function get_custom_js(): string {
        $scripts = '';
        foreach ($this->js_files as $val){
            if (file_exists($val)){
                $scripts .= '<script src='.$this->host.$val.'></script>';
            }
        }
        return $scripts;
    }

    /**
     * @return string an html link tag for each custom css files defined using view::set_view_params().
     */
    public function get_custom_css(): string {
        $scripts = '';
        foreach ($this->css_files as $val){
            $scripts .= '<link href='.$this->host.$val.' rel="stylesheet">';
        }
        return $scripts;
    }

    /**
     * @param $view string containing the name of the view file to be rendered.
     * @param $view_parameters array "title"=>String, "description"=>String, "js_files"=>[Paths to custom js files], "css"=>[Paths to custom css files]
     */
    public function render_view($view, $view_parameters = []): void {
        switch ($view){
            case '403':
                $view_parameters = ['title' => '403 - Access Denied'];
                break;
            case '404':
                $view_parameters = ['title' => '404 - Page Not Found'];
                break;
        }

        if(isset($view_parameters)) {
            $this->set_view_params($view_parameters);
        }

        $this->show_header = $this->show_header ?? true;
        $this->show_footer = $this->show_footer ?? true;

        $view = 'views/'. $view .'_view.php';

        if ($this->show_header){
            require 'views/includes/header_view.php';
        }

        if (file_exists($view))
        {
            require $view;
        }else{
            require 'views/404_view.php';
        }

        if ($this->show_footer){
            require 'views/includes/footer_view.php';
        }
    }
}

<!-- Begin Page Content -->
<div class="container-fluid">
    <?php if(!empty($this->errMessage)): ?>
        <div class="alert alert-warning" role="alert">
            <?= $this->errMessage ?>
        </div>
    <?php endif; ?>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?= $this->user['Username'] ?>'s profile</h1>
        <div class="button-group">
            <a href="/properties/" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-home fa-sm text-white-50 mr-1"></i> Manage properties</a>
        </div>
    </div>
    <div class="row">
        <!-- Left Section -->
        <div class="col-xl-3 col-lg-4">
            <!-- User's picture card -->
            <div class="card shadow mb-3">
                <!-- card header - dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Picture</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownmenulink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownmenulink">
                            <div class="dropdown-header">Actions:</div>
                            <a class="dropdown-item" data-toggle="modal" data-target="#picture-upload" href="">Upload picture</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#picture-delete" href="">Delete picture</a>
                        </div>
                    </div>
                </div>
                <!-- card body -->
                <div class="card-body p-1">
                    <form class="m-0" method="post" enctype="multipart/form-data">
                        <img src="<?= $this->user['Picture'] ?>" alt="<?= $this->user['FullName'] ?>" class="card-img">
                        <div class="modal fade" id="picture-upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <form class="m-0" method="post">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Upload your picture</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body bg-light py-5">
                                            <input class="form-control-file" name="Picture" type="file" accept="image/png, image/jpg, image/jpeg" required>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Upload</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="picture-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <form class="m-0" method="post">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Delete your picture</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body bg-light py-5">
                                            <p><strong>Are you sure you want to delete this picture?</strong></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button class="btn btn-primary">Delete</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- User's biography card -->
            <div class="card shadow mb-3">
                <a class="text-decoration-none" href="#user-bio" role="button" data-toggle="collapse" aria-haspopup="true" aria-expanded="false">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Biography</h6>
                    </div>
                </a>
                <div id="user-bio" class="collapse">
                    <form class="m-0" method="post">
                        <div class="card-body p-1">
                            <div class="input-group mb-2">
                                <textarea name="Biography" class="form-control" rows="8"><?= $this->user['Biography'] ?></textarea>
                            </div>
                        </div>
                        <div class="card-footer text-right p-2">
                            <input class="btn btn-light" type="reset" value="Reset">
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Right Section -->
        <div class="col-xl-9 col-lg-8">
            <!-- Personal info Card -->
            <div class="card shadow mb-3">
                <!-- card header - dropdown -->
                <a class="text-decoration-none" href="#personal-info" role="button" data-toggle="collapse" aria-haspopup="true" aria-expanded="false">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Personal details</h6>
                    </div>
                </a>
                <!-- card body -->
                <div id="personal-info" class="collapse show">
                    <form class="m-0" method="post">
                        <div class="card-body pb-4">
                            <!-- name and surname input group -->
                            <div class="input-group mb-2">
                                <div class="form-row col-sm-12">
                                    <label class="col-form-label col-sm-2" for="Name">Name:</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" name="Name" value="<?= $this->user['Name'] ?>">
                                    </div>
                                    <label class="col-form-label col-sm-2" for="Surname">Surname:</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" name="Surname" value="<?= $this->user['Surname'] ?>">
                                    </div>
                                </div>
                            </div>
                            <!-- Date of birth input group -->
                            <div class="input-group mb-2">
                                <div class="form-row col-sm-12">
                                    <label for="Birthdate" class="col-form-label col-sm-2">Date of birth:</label>
                                    <div class="col">
                                        <input class="form-control" type="date" name="Birthdate" value="<?= $this->user['Birthdate'] ?>">
                                    </div>
                                </div>
                            </div>
                            <!-- email input group -->
                            <div class="input-group mb-2">
                                <div class="form-row col-sm-12">
                                    <label class="col-form-label col-sm-2" for="Email">Email:</label>
                                    <div class="col">
                                        <input class="form-control" type="email" name="Email" value="<?= $this->user['Email'] ?>">
                                    </div>
                                </div>
                            </div>
                            <!-- phone and mobile input group -->
                            <div class="input-group mb-2">
                                <div class="form-row col-sm-12">
                                    <label class="col-form-label col-sm-2" for="Phone">Phone:</label>
                                    <div class="col">
                                        <input class="form-control" type="tel" name="Phone" value="<?= $this->user['Phone'] ?>" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}">
                                    </div>
                                    <label class="col-form-label col-sm-2" for="Mobile">Mobile:</label>
                                    <div class="col">
                                        <input class="form-control" type="tel" name="Mobile" value="<?= $this->user['Mobile'] ?>" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right p-2">
                            <input class="btn btn-light" type="reset" value="Reset">
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- Social Media Card -->
            <div class="card shadow mb-3">
                <a class="text-decoration-none" href="#social-media" role="button" data-toggle="collapse" aria-haspopup="true" aria-expanded="false">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Social Media</h6>
                    </div>
                </a>
                <div id="social-media" class="collapse">
                    <form class="m-0" method="post">
                        <div class="card-body py-3">
                            <div class="form-row">
                                <label class="sr-only" for="Facebook">Facebook:</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fab fa-facebook-square"></i></div>
                                    </div>
                                    <?php $facebook = (isset($this->user['Social']['Facebook']))?$this->user['Social']['Facebook']:''; ?>
                                    <input class="form-control" type="url" name="Facebook" value="<?= $facebook ?>">
                                </div>
                            </div>
                            <div class="form-row">
                                <label class="sr-only" for="Instagram">Instagram:</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fab fa-instagram"></i></div>
                                    </div>
                                    <?php $instagram = (isset($this->user['Social']['Instagram']))?$this->user['Social']['Instagram']:''; ?>
                                    <input class="form-control" type="url" name="Instagram" value="<?= $instagram ?>">
                                </div>
                            </div>
                            <div class="form-row">
                                <label class="sr-only" for="Twitter">Twitter:</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fab fa-twitter-square"></i></div>
                                    </div>
                                    <?php $twitter = (isset($this->user['Social']['Twitter']))?$this->user['Social']['Twitter']:''; ?>
                                    <input class="form-control" type="url" name="Twitter" value="<?= $twitter ?>">
                                </div>
                            </div>
                            <div class="form-row">
                                <label class="sr-only" for="Pinterest">Pinterest:</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fab fa-pinterest-square"></i></div>
                                    </div>
                                    <?php $pinterest = (isset($this->user['Social']['Pinterest']))?$this->user['Social']['Pinterest']:''; ?>
                                    <input class="form-control" type="url" name="Pinterest" value="<?= $pinterest ?>">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right p-2">
                            <input class="btn btn-light" type="reset" value="Reset">
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- User Settings Card -->
            <div class="card shadow mb-3">
                <!-- card header - dropdown -->
                <a class="text-decoration-none" href="#user-setting" role="button" data-toggle="collapse" aria-haspopup="true" aria-expanded="false">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">User settings</h6>
                    </div>
                </a>
                <!-- card body -->
                <div id="user-setting" class="collapse">
                    <form class="m-0" method="post">
                        <div id="user-setting" class="card-body collapse">
                            <!-- password and password verification input group -->
                            <div class="input-group mb-2">
                                <div class="form-row col-sm-12">
                                    <label class="col-form-label col-sm-2" for="Username">Username:</label>
                                    <div class="col">
                                        <input class="form-control" type="text" name="Username" value="<?= $this->user['Username'] ?>">
                                    </div>
                                </div>
                            </div>
                            <!-- password and password verification input group -->
                            <div class="input-group mb-2">
                                <div class="form-row col-sm-12">
                                    <label class="col-form-label col-sm-2" for="Password">Password:</label>
                                    <div class="col">
                                        <input class="form-control" type="password" name="Password">
                                    </div>
                                    <label class="col-form-label col-sm-2" for="PasswordVerify">Verify:</label>
                                    <div class="col">
                                        <input class="form-control" type="password" name="PasswordVerify">
                                    </div>
                                </div>
                            </div>
                            <!-- role and date of birth input group -->
                            <div class="input-group mb-2">
                                <div class="form-row col-sm-12">
                                    <label class="col-form-label col-sm-2" for="Role">Role:</label>
                                    <div class="col">
                                        <?php $disabled = ($this->user['Role']['Access'] < 4) ? 'disabled' : ''; ?>
                                        <select class="form-control" name="Role" <?= $disabled ?>>
                                            <?php foreach ($this->roles as $role): ?>
                                                <?php $selected = ($role['ID'] === $this->user['Role']['ID']) ? 'selected' : ''; ?>
                                                <option value="<?= $role['ID'] ?>" <?= $selected ?>><?= $role['Name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right p-2">
                            <input class="btn btn-light" type="reset" value="Reset">
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
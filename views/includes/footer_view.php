    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; Your Website 2019</span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Including the logout modal -->
    <?php require_once 'views/modals/logout_modal.php'; ?>

    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo $this->host ?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo $this->host ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?php echo $this->host ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?php echo $this->host ?>assets/js/sb-admin-2.min.js"></script>

    <!-- Custom scripts for this view-->
    <?php echo $this->get_custom_js() ?>

</body>

</html>
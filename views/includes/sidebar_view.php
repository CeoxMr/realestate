    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../index_view.php">
            <div class="sidebar-brand-icon">
                <i class="fas fa-building"></i>
            </div>
            <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <?php foreach ($this->sidebar as $section): ?>
            <?php if($section['access'] <= $_SESSION['users']['Role']['Access']): ?>
                <?php if(array_key_exists('child', $section)): ?>
                    <li class="nav-item">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse<?=$section['name']?>" aria-expanded="false" aria-controls="collapsePages">
                            <i class="fas <?= $section['icon'] ?? NULL ?>"></i>
                            <span><?= $section['title'] ?? NULL ?></span>
                        </a>
                        <div id="collapse<?=$section['name']?>" class="collapse" aria-labelledby="heading" data-parent="#accordionSidebar" style="">
                            <div class="bg-white py-2 collapse-inner">
                                <?php foreach($section['child'] as $child): ?>
                                    <a class="collapse-item" href="<?= $child['href']?>">
                                        <i class="fas <?= $child['icon'] ?? NULL ?> mr-2"></i>
                                        <span><?= $child['title'] ?></span>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </li>
                <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $section['href'] ?? NULL ?>">
                            <i class="fas <?= $section['icon'] ?? NULL ?>"></i>
                            <span><?= $section['title'] ?? NULL ?></span></a>
                    </li>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-1 text-gray-800"><?= $this->title ?></h1>
        <?php if(!empty($this->description)):?>
            <p class="mb-4"><?= $this->description ?></p>
        <?php endif; ?>
    </div>
    <!-- /.container-fluid -->

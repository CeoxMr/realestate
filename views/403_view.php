<!-- Begin Page Content -->
<div class="container-fluid">
    <?php if(!empty($this->errMessage)): ?>
        <div class="alert alert-warning" role="alert">
            <?= $this->errMessage ?>
        </div>
    <?php endif; ?>
    <!-- 403 Error Text -->
    <div class="text-center">
        <div class="error mx-auto" data-text="403">403</div>
        <p class="lead text-gray-800 mb-4">Access Denied</p>
        <p class="text-gray-500 mb-0">It looks like you don't have access to this page, contact your system administrator...</p>
        <a href="/">&larr; Back to Dashboard</a>
    </div>
</div>
<!-- /.container-fluid -->
